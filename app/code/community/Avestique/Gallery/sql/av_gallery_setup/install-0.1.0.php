<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Gallery
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file ${FILE_NAME}
 */
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$entityTypes = Mage::getSingleton('av_gallery/eav_config')->getEntityTypes();

foreach($entityTypes as $entityType)
{
    $installer->addAttribute($entityType['entity_type_code'], 'custom_media_image', array(
        'type'                       => 'varchar',
        'label'                      => 'Custom Image',
        'input'                      => 'av_media_image',
        'frontend'                   => 'av_gallery/attribute_frontend_image',
        'backend'                    => 'av_gallery/attribute_backend_media',
        'required'                   => false,
        'sort_order'                 => 1,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'group'                      => 'Images',
    ));
}

/**
 * Create table 'av_gallery/product_attribute_media_gallery'
 */

$installer->run("DROP TABLE IF EXISTS `{$installer->getTable('av_gallery/attribute_media_gallery')}`;");

$table = $installer->getConnection()
    ->newTable($installer->getTable('av_gallery/attribute_media_gallery'))
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Value ID')
    ->addColumn('attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Attribute ID')
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Entity ID')
    ->addColumn('entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Entity Type ID')
    ->addColumn('value', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
    ), 'Value')
    ->addIndex($installer->getIdxName('av_gallery/attribute_media_gallery', array('entity_type_id')),
        array('entity_type_id'))
    ->addIndex($installer->getIdxName('av_gallery/attribute_media_gallery', array('attribute_id')),
        array('attribute_id'))
    ->addIndex($installer->getIdxName('av_gallery/attribute_media_gallery', array('entity_id')),
        array('entity_id'))
    ->addForeignKey(
        $installer->getFkName(
            'av_gallery/attribute_media_gallery',
            'attribute_id',
            'eav/attribute',
            'attribute_id'
        ),
        'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey(
        $installer->getFkName(
            'av_gallery/attribute_media_gallery',
            'entity_type_id',
            'eav/entity_type',
            'entity_type_id'
        ),
        'entity_type_id', $installer->getTable('eav/entity_type'), 'entity_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Media Gallery Attribute Backend Table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'av_gallery/product_attribute_media_gallery_value'
 */
$installer->run("DROP TABLE IF EXISTS `{$installer->getTable('av_gallery/attribute_media_gallery_value')}`;");

$table = $installer->getConnection()
    ->newTable($installer->getTable('av_gallery/attribute_media_gallery_value'))
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
    ), 'Value ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
    ), 'Store ID')
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
    ), 'Label')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
    ), 'Position')
    ->addColumn('disabled', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Is Disabled')
    ->addIndex($installer->getIdxName('av_gallery/attribute_media_gallery_value', array('store_id')),
        array('store_id'))
    ->addForeignKey(
        $installer->getFkName(
            'av_gallery/attribute_media_gallery_value',
            'value_id',
            'av_gallery/attribute_media_gallery',
            'value_id'
        ),
        'value_id', $installer->getTable('av_gallery/attribute_media_gallery'), 'value_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey(
        $installer->getFkName(
            'av_gallery/attribute_media_gallery_value',
            'store_id',
            'core/store',
            'store_id'
        ),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Media Gallery Attribute Value Table');
$installer->getConnection()->createTable($table);


$installer->endSetup();