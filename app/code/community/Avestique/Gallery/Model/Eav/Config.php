<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Gallery
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Config.php
 */

class Avestique_Gallery_Model_Eav_Config extends Mage_Eav_Model_Config
{
    /**
     * Get entity types
     *
     * @return array
     */
    public function getEntityTypes()
    {
        $this->_initEntityTypes();

        return $this->_entityData;
    }
}