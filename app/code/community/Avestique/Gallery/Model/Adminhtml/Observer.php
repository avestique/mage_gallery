<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Gallery
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Observer.php
 */

class Avestique_Gallery_Model_Adminhtml_Observer
{
    /**
     * core_block_abstract_to_html_before event
     * http://screencast.com/t/FjIUBes2M2WP
     *
     * @var Mage_Adminhtml_Block_Catalog_Product_Attribute_New_Product_Attributes
     * @var Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Attributes
     * @var Mage_Core_Block_Abstract
     * @param $event
     */
    public function removeAttribute($event)
    {
        if ($event->getEvent()->getBlock() instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Attributes)
        {
            if ($attributes = $event->getEvent()->getBlock()->getGroupAttributes() )
            {
                foreach($attributes as $key => $attribute)
                {
                    if ($attribute->getAttributeCode() == 'custom_media_image')
                    {
                        unset($attributes[$key]);
                    }
                }

                $event->getEvent()->getBlock()->setGroupAttributes($attributes);
            }
        }

    }

    /**
     * adminhtml_catalog_product_edit_element_types event
     * http://screencast.com/t/grNHw9p80RE
     *
     * @var Mage_Adminhtml_Block_Catalog_Product_Attribute_New_Product_Attributes
     * @var Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Attributes
     * @param $event
     */
    public function prepareTypes($event)
    {
        /** @var Varien_Object $response */
        $response = $event->getEvent()->getResponse();

        /** @var array $types */
        $types = $response->getTypes();
        $types['av_media_image'] = Mage::getConfig()->getBlockClassName('av_gallery/adminhtml_form_helper_gallery');

        $response->setTypes($types);
    }

    /**
     * adminhtml_block_html_before event
     * @var Mage_Adminhtml_Block_Template
     * @var Mage_Adminhtml_Block_Customer_Edit_Tab_Carts
     * @param $event
     */
    public function addImageField($event)
    {
        /** редактируемое условие на добавление поля в закладку */
        if ($event->getEvent()->getBlock() instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Attributes)
        {
            /** @var Varien_Data_Form $form */
            $form = $event->getEvent()->getBlock()->getForm();

            foreach($form->getElements() as $el){
                if ($el->getLegend()=='Images')
                {
                    $fieldsetImages = $form->addFieldset('images1', array(
                        'legend' => Mage::helper('av_gallery')->__('Images1'),
                        'class' => 'fieldset-wide'
                    ));

                    $fieldsetImages->addType('av_media_image', Mage::getConfig()->getBlockClassName('av_gallery/adminhtml_form_helper_gallery'));

                    $data = Mage::registry('product')->getData();

                    $values = array();

                    if (!empty($data['custom_media_image']))
                    {
                        $values = $data['custom_media_image'];
                    }

                    $fieldsetImages->addField("custom_media_image", 'av_media_image',array(
                        'name'      => "custom_media_image",
                        'label'     => 'Custom Media Gallery',
                        'class'     => '',
                        'required'  => false,
                        'note'      => '',
                        'id'   => 1,
                        'value' => $values
                    ));//->addElementValues($values);//->setAttributesList(array(10 => 'small_image'));
                }
            }
        }
    }

    public function saveGallery($event)
    {
        $object = $event->getEvent()->getObject();

        //echo '<pre><br/>'; var_dump($object->debug()); die();
    }
}