<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Gallery
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Image.php
 */

class Avestique_Gallery_Model_Attribute_Frontend_Image extends Mage_Eav_Model_Entity_Attribute_Frontend_Abstract
{
    public function getUrl($object, $size=null)
    {
        $url = false;
        $image = $object->getData($this->getAttribute()->getAttributeCode());

        if( !is_null($size) && file_exists(Mage::getBaseDir('media').DS.Mage::getModel('av_gallery/config')->getBaseMediaPathAddition().DS.$size.DS.$image) ) {
            # resized image is cached
            $url = Mage::app()->getStore($object->getStore())->getBaseUrl('media') . Mage::getModel('av_gallery/config')->getBaseMediaUrlAddition() . '/' . $size . '/' . $image;
        } elseif( !is_null($size) ) {
            # resized image is not cached
            $url = Mage::app()->getStore($object->getStore())->getBaseUrl() . Mage::getModel('av_gallery/config')->getBaseMediaUrlAddition() . '/image/size/' . $size . '/' . $image;
        } elseif ($image) {
            # using original image
            $url = Mage::app()->getStore($object->getStore())->getBaseUrl('media'). Mage::getModel('av_gallery/config')->getBaseMediaUrlAddition() . '/'.$image;
        }
        return $url;
    }
}