<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Gallery
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Gallery.php
 */

class Avestique_Gallery_Block_Adminhtml_Form_Helper_Gallery extends Mage_Adminhtml_Block_Catalog_Product_Helper_Form_Gallery
{
    /**
     * Prepares content block
     *
     * @return string
     */
    public function getContentHtml()
    {
        /* @var $content Avestique_Gallery_Block_Adminhtml_Form_Helper_Gallery_Content */
        $content = Mage::getSingleton('core/layout')
            ->createBlock('av_gallery/adminhtml_form_helper_gallery_content');

        $content->setId($this->getHtmlId() . '_content')
                ->setElement($this);

        return $content->toHtml();
    }

    /**
     * Check "Use default" checkbox display availability
     *
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * @return bool
     */
    public function canDisplayUseDefault($attribute)
    {
        if ($this->getDataObject()->getStoreId()) {
            return true;
        }

        return false;
    }

    /**
     * Check readonly attribute
     *
     * @param Mage_Eav_Model_Entity_Attribute|string $attribute
     * @return boolean
     */
    public function getAttributeReadonly($attribute)
    {
        if (is_object($attribute)) {
            $attribute = $attribute->getAttributeCode();
        }

        if ($this->getDataObject()->isLockedAttribute($attribute)) {
            return true;
        }

        return false;
    }

    /**
     * Retrieve label of attribute scope
     *
     * GLOBAL | WEBSITE | STORE
     *
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * @return string
     */
    public function getScopeLabel($attribute)
    {
        $html = '';
        if (Mage::app()->isSingleStoreMode()) {
            return $html;
        }

        /*
        if ($attribute->isScopeGlobal()) {
            $html .= '<br/>' . Mage::helper('adminhtml')->__('[GLOBAL]');
        } elseif ($attribute->isScopeWebsite()) {
            $html .= '<br/>' . Mage::helper('adminhtml')->__('[WEBSITE]');
        } elseif ($attribute->isScopeStore()) {
            $html .= '<br/>' . Mage::helper('adminhtml')->__('[STORE VIEW]');
        }*/
        return $html;
    }
}