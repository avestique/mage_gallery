<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Gallery
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Content.php
 */

class Avestique_Gallery_Block_Adminhtml_Form_Helper_Gallery_Content extends Avestique_Gallery_Block_Adminhtml_Catalog_Product_Helper_Form_Gallery_Content
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('avestique/gallery/media/helper/gallery.phtml');
    }

    protected function _prepareLayout()
    {
        $uploaderBlock = $this->getLayout()->createBlock('adminhtml/media_uploader');
        $uploaderBlock->setTemplate('avestique/gallery/media/uploader.phtml');

        $this->setChild('uploader', $uploaderBlock);

        $this->getUploader()->getConfig()
            ->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('av_gallery/index/upload'))
            ->setFileField('image')
            ->setFilters(array(
                'images' => array(
                    'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
                    'files' => array('*.gif', '*.jpg','*.jpeg', '*.png')
                )
            ));

        Mage::dispatchEvent('av_gallery_layer_prepare_layout', array('block' => $this));
    }

    /**
     * Enter description here...
     *
     * @return array
     */
    public function getImageTypes()
    {
        $imageTypes = array();
        foreach ($this->getMediaAttributes() as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
            $imageTypes[$attribute->getAttributeCode()] = array(
                'label' => $attribute->getFrontend()->getLabel() . ' '
                    . Mage::helper('catalog')->__($this->getElement()->getScopeLabel($attribute)),
                'field' => $attribute->getAttributeCode()//$this->getElement()->getAttributeFieldName($attribute)
            );
        }

        return $imageTypes;
    }

    public function getImagesValuesJson()
    {
        $values = array();
        foreach ($this->getMediaAttributes() as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
            $values[$this->getElement()->getId()] = $this->getElement()->getDataObject()->getData(
                $attribute->getAttributeCode()
            );
        }
        return Mage::helper('core')->jsonEncode($values);
    }

    public function getImagesJson()
    {
        if(is_array($this->getElement()->getValue())) {
            $value = $this->getElement()->getValue();

            if(count($value['images'])>0) {
                foreach ($value['images'] as &$image) {
                    $image['url'] = Mage::getSingleton('av_gallery/config')
                        ->getMediaUrl($image['file']);
                }

                return Mage::helper('core')->jsonEncode($value['images']);
            }
        }
        return '[]';
    }
}