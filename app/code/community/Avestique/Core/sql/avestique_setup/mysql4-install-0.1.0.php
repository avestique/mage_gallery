<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file mysql4-install-1.0.0.php
 */

$this->startSetup();

$v = Mage::getStoreConfig('av_core/feed/installed');

if (!$v){
    Mage::getModel('core/config_data')
        ->setScope('default')
        ->setPath('av_core/feed/installed')
        ->setValue(time())
        ->save();
}

$this->endSetup();