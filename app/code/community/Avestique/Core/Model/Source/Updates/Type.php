<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Base
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Type.php
 */  
class Avestique_Core_Model_Source_Updates_Type extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	const TYPE_NOTIFICATION_PROMO            = 'PROMO';
	const TYPE_NOTIFICATION_NEW_RELEASE      = 'NEW_RELEASE';
	const TYPE_NOTIFICATION_UPDATE_RELEASE   = 'UPDATE_RELEASE';
	const TYPE_NOTIFICATION_ADDITIONAL_INFO  = 'ADDITIONAL_INFO';
	const TYPE_NOTIFICATION_INSTALLED_UPDATE = 'INSTALLED_UPDATE';
	
	
	public function toOptionArray()
	{
	    $hlp = Mage::helper('av_core');
		return array(
			array('value' => self::TYPE_NOTIFICATION_INSTALLED_UPDATE, 'label' => $hlp->__('My extensions updates')),
			array('value' => self::TYPE_NOTIFICATION_UPDATE_RELEASE,   'label' => $hlp->__('All extensions updates')),
			array('value' => self::TYPE_NOTIFICATION_NEW_RELEASE,      'label' => $hlp->__('New Releases')),
			array('value' => self::TYPE_NOTIFICATION_PROMO,            'label' => $hlp->__('Promotions/Discounts')),
			array('value' => self::TYPE_NOTIFICATION_ADDITIONAL_INFO,  'label' => $hlp->__('Other information'))
		);
	}
	
	/**
     * Retrive all attribute options
     *
     * @return array
     */
    public function getAllOptions()
    {
    	return $this->toOptionArray();
	}
	
	
	/**
	 * Returns label for value
	 * @param string $value
	 * @return string
	 */
	public function getLabel($value)
	{
		$options = $this->toOptionArray();
		foreach($options as $v){
			if($v['value'] == $value){
				return $v['label'];
			}
		}
		return '';
	}
	
	/**
	 * Returns array ready for use by grid
	 * @return array 
	 */
	public function getGridOptions()
	{
		$items = $this->getAllOptions();
		$out = array();
		foreach($items as $item){
			$out[$item['value']] = $item['label'];
		}
		return $out;
	}
}