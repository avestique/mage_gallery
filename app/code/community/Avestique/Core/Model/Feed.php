<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Base
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Feed.php
 */

class Avestique_Core_Model_Feed extends Mage_AdminNotification_Model_Feed
{
    const XML_FREQUENCY_PATH    = 'av_core/feed/check_frequency';
    const XML_LAST_UPDATE_PATH  = 'av_core/feed/last_update';
    const XML_USER_NOTIFICATION = 'av_core/feed/user_notifications
    ';
    
    const URL_EXTENSIONS  = 'http://demo.autocensor.avq/_html/feed-extensions.xml';
    const URL_NEWS        = 'http://demo.autocensor.avq/_html/feed-news.xml';

	
	public static function check()
	{
		return Mage::getModel('av_core/feed')->checkUpdate();
	}
	
    public function checkUpdate()
    {
        //if (($this->getFrequency() + $this->getLastUpdate()) > time()) { return $this; }

        $this->setLastUpdate();

        if (!extension_loaded('curl')) {
            return $this;
        }

        // load all new and relevant updates into inbox
        $feedData   = array();
        $feedXml = $this->getFeedData();

        $wasInstalled = gmdate('Y-m-d H:i:s', Mage::getStoreConfig('av_core/feed/installed'));

        if ($feedXml && $feedXml->channel && $feedXml->channel->item)
        {
            foreach ($feedXml->channel->item as $item) {
                $date = $this->getDate((string)$item->pubDate);

                // compare strings, but they are well-formmatted 
                if ($date < $wasInstalled){
                    continue;
                }
                if (!$this->isTypeNotificationSelected($item)){
                    continue;
                }
                    
                $feedData[] = array(
                    'severity'      => 3,
                    'date_added'    => $this->getDate($date),
                    'title'         => (string)$item->title,
                    'description'   => (string)$item->description,
                    'url'           => (string)$item->link,
                );
            }
            if ($feedData) {
                Mage::getModel('adminnotification/inbox')->parse($feedData);
            }
        }

        //load all available extensions in the cache
        $this->_feedUrl = self::URL_EXTENSIONS;
        $feedData   = array();
        $feedXml = $this->getFeedData();
        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            foreach ($feedXml->channel->item as $item) {
                $feedData[(string)$item->code] = array(
                    'name'    => (string)$item->title,
                    'url'     => (string)$item->link,
                    'version' => (string)$item->version,
                );
            }
            if ($feedData) {
                Mage::app()->saveCache(serialize($feedData), 'av_core_extensions');
            }
        }
        
        return $this;
    }

    public function getFrequency()
    {
        return Mage::getStoreConfig(self::XML_FREQUENCY_PATH);
    }

    public function getLastUpdate()
    {
        return Mage::app()->loadCache('av_core_notifications_lastcheck');
    }
 
    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), 'av_core_notifications_lastcheck');
        return $this;
    }
    
    public function getFeedUrl()
    {
        if (is_null($this->_feedUrl)) {
            $this->_feedUrl = self::URL_NEWS;
        }
        $query = '?s=' . urlencode(Mage::getStoreConfig('web/unsecure/base_url')); 
        return $this->_feedUrl  . $query;
    }
    
    protected function getUserNotification()
	{
		return Mage::getStoreConfig(self::XML_USER_NOTIFICATION);
	}

    /**
     * User type notification
     *
     * @param $item
     * @return bool
     */
    protected function isTypeNotificationSelected($item)
	{
		$userNotifications = @explode(',', $this->getUserNotification());
		$types             = @explode(':', (string)$item->type);
		$extension         = (string)$item->extension;
		
		$selfUpgrades = array_search(Avestique_Core_Model_Source_Updates_Type::TYPE_NOTIFICATION_INSTALLED_UPDATE, $types);
		
		foreach ($types as $type){
			if (array_search($type, $userNotifications) !== false){
				return true;
			}
			
			if ($extension && ($type == Avestique_Core_Model_Source_Updates_Type::TYPE_NOTIFICATION_UPDATE_RELEASE) && $selfUpgrades){
                if ($this->isInstalled($extension)){
                	return true;
                }
			}
		}
		
		return false;
	}

    /**
     * Is extension installed
     *
     * @param $code
     * @return bool
     */
    protected function isInstalled($code)
	{
		$modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        foreach ($modules as $moduleName) {
        	if ($moduleName == $code){
        		return true;
        	}
        }
        
		return false;
	}
    
}